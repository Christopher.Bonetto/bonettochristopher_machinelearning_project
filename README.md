**Intro**

This project was developed for my school specialization and for my passion to implement the AI behaviors.
After experimenting with state machines, command patterns and behavior trees I decided to create my first project using reinforcement learning.
In this project the agent must learn how to survive in different enemy-filled arenas that fit the agent's training.
This will learn how and when to flee, aim and shoot and when its learning reaches thresholds the number of enemies, their speed will increase.
This growth of the environment is possible thanks to the use of the addition of the curriculum within the training


**Engine/Plugin**

Engine:
- Unity -> version 2020.2.5f1

Package Manager on Unity:
- ML Agents -> version 1.0.7
- TextMeshPro -> version 3.0.1


**External:**
- Python Libraries -> version 3.7.9
- PyTorch -> version 1.7
- ML-Agents -> version 10



**Result:**
- https://drive.google.com/file/d/1m5zg4OTR54MgyXj2WISqVT988SA_624_/view
